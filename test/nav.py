import dash
from dash import html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output

# Initialiser l'application Dash
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

# Définir la barre de navigation
navbar = dbc.NavbarSimple(
    children=[
        dbc.NavItem(dbc.NavLink("Page 1", href="/page-1")),
        dbc.NavItem(dbc.NavLink("Page 2", href="/page-2")),
    ],
    brand="Mon Dashboard",
    brand_href="/",
    color="primary",
    dark=True,
)

# Définir le layout de l'application avec la barre de navigation
app.layout = html.Div([
    navbar,
    # Contenu de la page ici
])

if __name__ == "__main__":
    app.run_server(debug=True)
