import dash
from dash import dcc, html
import dash_bootstrap_components as dbc
from dash import Dash, html, dash_table, dcc, callback, Output, Input, State
import pandas as pd
import plotly.graph_objs as go
import numpy as np
import plotly.express as px
from scipy.stats import gaussian_kde
import plotly.figure_factory as ff
from plotly.subplots import make_subplots

#-------------------------------------------------------------------------- PRÉPARATION DE LA DONNÉÉ --------------------------------------------------------------- #

df_initial = pd.read_excel("Daily-Life Gait Quality.xlsx")

selected_variables = [
    "Number of locomotion bouts",
    "Duration of locomotion",
    "Number of strides",
    "Duration of lying",
    "Duration of sitting",
    "Duration of standing",
    "Number of transfers",
    "Age",
    "Male gender",
    "Living independently (0=yes, 1=assited living, 2=residential care)",
    "Living with partner",
    "Cognitive function (MMSE score)",
    "Frequently experiencing dizziness ",
    "Inability to use public transportation",
    "Inability to descent and ascent a stair with 15 steps",
    "Inability to clip own toenails",
    "Weight (kg)",
    "Length (cm)",
    "Having a pet",
    "Frequent alcohol consumption (more than 15 glases a week)",
    "Followed higher education (more than 11 years)",
    "Depression (GDS)",
    "Use of a walking aid",
    "Fear of falling (FESi)",
    "Number of falls in month 1",
    "Number of falls in month 2",
    "Number of falls in month 3",
    "Number of falls in month 4",
    "Number of falls in month 5",
    "Number of falls in month 6",
    "Number of falls in month 7",
    "Number of falls in month 8",
    "Number of falls in month 9",
    "Number of falls in month 10",
    "Number of falls in month 11",
    "Number of falls in month 12",
    "Number of falls in past 6 months",
    "Number of falls in past year"
]

df = df_initial[selected_variables]

#----------------------------------------------------------------- CREÉATION DE SOUS CATÉGORIES DE DONNÉÉS ------------------------------------------------------- #
dropdown_options_mobility = ["Number of locomotion bouts",
    "Duration of locomotion",
    "Number of strides",
    "Duration of lying",
    "Duration of sitting",
    "Duration of standing",
    "Number of transfers",
    "Number of falls in past 6 months",
    "Number of falls in past year"]

dropdown_options_Demographie = ["Age",
    "Male gender",
    "Living independently (0=yes, 1=assited living, 2=residential care)",
    "Living with partner","Having a pet",
    "Followed higher education (more than 11 years)",
    "Use of a walking aid",
    "Number of falls in past 6 months",
    "Number of falls in past year"]

#-------------------------------------------------------------------- CREÉATION DU DASH ---------------------------------------------------------------------- #
# Thème Bootstrap pour le style de l'application
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP], suppress_callback_exceptions=True)

#------------------------------------------------------------------ FONCTIONS D'AFFICHAGE -------------------------------------------------------------------- #
def customize_figure(fig, chart_bgcolor="#FFFFFF"):  
    fig.update_layout(
        plot_bgcolor=chart_bgcolor, 
        paper_bgcolor="#F0F2F6", 
        font=dict(color='#007BFF', size=12)
    )

    fig.update_xaxes(showline=True, linewidth=2, linecolor='black', gridcolor='lightgrey')
    fig.update_yaxes(showline=True, linewidth=2, linecolor='black', gridcolor='lightgrey')

    fig.update_traces(marker=dict(line=dict(width=2, color='DarkSlateGrey')),
                      selector=dict(type='histogram'))

    if 'pie' in [trace.type for trace in fig.data]:
        fig.update_traces(textinfo='percent+label', textfont=dict(color='black'))
    else:
        fig.update_traces(texttemplate='%{y}', textposition='outside')

    return fig

def customize_figure2(fig, chart_bgcolor="#FFFFFF"):
    fig.update_layout(
        plot_bgcolor=chart_bgcolor,
        paper_bgcolor="#F0F2F6",
        font=dict(color='#007BFF', size=12),
    )
    fig.update_xaxes(showline=True, linewidth=2, linecolor='black', gridcolor='lightgrey')
    fig.update_yaxes(showline=True, linewidth=2, linecolor='black', gridcolor='lightgrey')
    fig.update_traces(marker=dict(line=dict(width=2, color='DarkSlateGrey')),
                      selector=dict(type='histogram'))
    return fig


#--------------------------------------------------------------------------- PARTIE ACCUEIL ------------------------------------------------------------------- #

df['BMI'] = df['Weight (kg)'] / ((df['Length (cm)'] / 100) ** 2)

# Sélection des variables considérées comme fiables et calcul des moyennes
reliable_variables = [
    "Age",
    "Weight (kg)",
    "Length (cm)",
    "BMI"
]

# Calcul des moyennes pour les variables sélectionnées
mean_values = df[reliable_variables].mean().reset_index()
mean_values.columns = ['Variable', 'Moyenne']

# Formatage des moyennes pour une meilleure lisibilité
mean_values['Moyenne'] = mean_values['Moyenne'].round(2)

def generate_color_card(name, value, color):
    """
    Génère une carte de couleur pour afficher une statistique clé.

    :param name: Nom de la variable
    :param value: Valeur associée
    :param color: Couleur de fond de la carte
    :return: Un objet html.Div représentant la carte
    """
    return dbc.Card(
        dbc.CardBody([
            html.H5(name, className="card-title", style={'textAlign': 'center'}),
            html.P(f"{value}", className="card-text", style={'textAlign': 'center', 'fontSize': '20px'}),
        ]),
        style={'backgroundColor': color, 'color': 'white', 'marginTop': '10px', 'marginBottom': '10px'},
        className="mb-3"
    )


total_patients = len(df)
colors = ['#007BFF', '#FFC107', '#28A745', '#DC3545', '#17A2B8']
variable_names = ["Age moyen", "Poids moyen (kg)", "Taille moyenne (cm)", "IMC moyen"]
cards = [generate_color_card(name, f"{value}", color) for (name, value), color in zip(zip(variable_names, mean_values['Moyenne']), colors)]
cards.append(generate_color_card('Nombre de Patients', str(total_patients), '#FF851B'))

#------------------------------------------------------- PARTIE ACTIVITE PHYSIQUE ET MOBILITE ------------------------------------------------------- #

# -------------------------------------------------------------- EPISODES LOCOMOTION ---------------------------------------------------------------- #
data = df['Number of locomotion bouts'].dropna() 

kde = gaussian_kde(data)
x_range = np.linspace(data.min(), data.max(), 500)  
density = kde(x_range) * len(data) * (data.max() - data.min()) / 500 

fig_locomotion_bouts = go.Figure()

fig_locomotion_bouts.add_trace(go.Histogram(x=data, name='Nombre d\'épisodes', marker_color='#007BFF', opacity=0.75))
fig_locomotion_bouts.add_trace(go.Scatter(x=x_range, y=density, name='Densité', line=dict(color='#FF5733', width=2), yaxis='y2'))

fig_locomotion_bouts.update_xaxes(title_text='', showgrid=False)
fig_locomotion_bouts.update_yaxes(title_text='Nombre', side='left', showgrid=True, gridcolor='lightgray')

fig_locomotion_bouts.update_layout(
    yaxis2=dict(title='Densité', overlaying='y', side='right', showgrid=False),
    title=dict(text='Nombre d\'épisodes de locomotion', x=0.5, font=dict(size=18)),
    plot_bgcolor='white',
    bargap=0.2
)
fig_locomotion_bouts.update_layout(legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01, bgcolor='rgba(255,255,255,0.9)'))

# ---------------------------------------------------------------------- TRANSFERTS ------------------------------------------------------------------- #
low_threshold = df['Number of transfers'].describe()['25%'] -10
high_threshold = df['Number of transfers'].describe()['75%'] + 10

labels = ['Faible', 'Moyen', 'Elevé']  

bins = [df['Number of transfers'].min(), low_threshold, high_threshold, df['Number of transfers'].max()]
df['Transfers_Category'] = pd.cut(df['Number of transfers'], bins=bins, labels=labels, include_lowest=True)

transfers_binned = df['Transfers_Category'].value_counts().sort_index()

fig_transfers = px.bar(
    x=transfers_binned.index, 
    y=transfers_binned.values,
    title='Nombre de transferts par catégorie',
    color_discrete_sequence=['#EF553B']
)

fig_transfers.update_layout(
    xaxis_title="",
    yaxis_title="",
    xaxis={'showticklabels': True},
    yaxis={'showticklabels': True}
)

fig_transfers.update_yaxes(showgrid=True, gridcolor='lightgray')

# ---------------------------------------------------------------- PAS / DUREE LOCOMOTION ---------------------------------------------------------------- #
fig_steps_vs_duration = px.scatter(df, x='Number of strides', y='Duration of locomotion',
                                   title='Nombre de pas en fonction du nombre d\'épisodes de locomotion',
                                   color_discrete_sequence=['#00CC96'])

fig_steps_vs_duration.update_xaxes(title_text='Nombre de pas', showgrid=False)
fig_steps_vs_duration.update_yaxes(title_text='Durée de locomotion', showgrid=True, gridcolor='lightgray')

fig_steps_vs_duration.update_layout(margin=dict(t=60), plot_bgcolor='white', title_x=0.5, title_font=dict(size=18))
fig_steps_vs_duration.update_layout(legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01, bgcolor='rgba(255,255,255,0.9)'))

# ------------------------------------------------------------------- DUREE POSTURES ---------------------------------------------------------------------- #
fig_posture_duration = go.Figure()
fig_posture_duration.add_trace(go.Pie(labels=['Debout', 'Assis', 'Couché'],
                                      values=[df['Duration of standing'].sum(),
                                              df['Duration of sitting'].sum(),
                                              df['Duration of lying'].sum()],
                                      hole=.3))

fig_posture_duration.update_layout(title_text='Répartition des durées des postures')
fig_posture_duration.update_layout(margin=dict(t=60), plot_bgcolor='white', title_x=0.5, title_font=dict(size=18))
fig_posture_duration.update_layout(legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01, bgcolor='rgba(255,255,255,0.9)'))


#--------------------------------------------------------- PARTIE DEMOGRAPHIE ET STYLE DE VIE ------------------------------------------------------------- #
# figure pour l'histogramme de distribution de l'âge
# Préparation des données pour l'histogramme de l'âge
data_age = df['Age'].dropna() 

# Calcul de la densité pour l'âge
kde_age = gaussian_kde(data_age)
x_range_age = np.linspace(data_age.min(), data_age.max(), 500)
density_age = kde_age(x_range_age) * len(data_age) * (data_age.max() - data_age.min()) / 500 

# Création de la figure pour l'âge avec l'histogramme et la courbe de densité
fig_age_distribution = go.Figure()

# Ajout de l'histogramme pour l'âge
fig_age_distribution.add_trace(go.Histogram(x=data_age, name='Distribution de l\'âge', marker_color='#007BFF', opacity=0.75))

# Ajout de la courbe de densité pour l'âge sur un axe des ordonnées secondaire
fig_age_distribution.add_trace(go.Scatter(x=x_range_age, y=density_age, name='Densité', line=dict(color='#FF5733', width=2), yaxis='y2'))

# Mise à jour des paramètres d'axe et de style pour la distribution de l'âge
fig_age_distribution.update_xaxes(title_text='Âge', showgrid=False)
fig_age_distribution.update_yaxes(title_text='Nombre', side='left', showgrid=True, gridcolor='lightgray')

fig_age_distribution.update_layout(
    yaxis2=dict(title='Densité', overlaying='y', side='right', showgrid=False),
    title=dict(text='Distribution de l\'âge', x=0.5, font=dict(size=18)),
    plot_bgcolor='white',
    bargap=0.2
)
fig_age_distribution.update_layout(legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01, bgcolor='rgba(255,255,255,0.9)'))

# Calcul des proportions pour la situation de vie
living_situation_counts = df["Living independently (0=yes, 1=assited living, 2=residential care)"].value_counts()
labels = ['Indépendant', 'Vie Assistée', 'Soins Résidentiels']
values = living_situation_counts.values

fig_living_situation = go.Figure(data=[go.Pie(labels=labels, values=values)])

fig_living_situation.update_layout(title_text='Situation de Vie')
fig_living_situation.update_yaxes(showgrid=True, gridcolor='lightgray')
fig_living_situation.update_layout(margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))

# Calcul des proportions pour le niveau d'éducation
education_counts = df["Followed higher education (more than 11 years)"].value_counts()
education_fig = px.pie(values=education_counts.values, names=['Supérieur', 'Inférieur'], title="Éducation")
education_fig.update_yaxes(showgrid=True, gridcolor='lightgray')
education_fig.update_layout(margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))

# Calcul des proportions pour la vie avec un partenaire
partner_counts = df["Living with partner"].value_counts()
partner_fig = px.pie(values=partner_counts.values, names=['Avec Partenaire', 'Sans Partenaire'], title="Situation conjugale")
partner_fig.update_yaxes(showgrid=True, gridcolor='lightgray')
partner_fig.update_layout(margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))


#--------------------------------------------------------------------- PARTIE SANTE ET BIEN ETRE ----------------------------------------------------------- #
fig= px.scatter(
    df,
    x='Age',
    y='Cognitive function (MMSE score)',
    title='Fonction cognitive (score MMSE) par rapport à l\'âge',
    color='Age',
    labels={'Age': 'Age', 'Cognitive function (MMSE score)': 'MMSE Score'}
)

fig.update_yaxes(showgrid=True, gridcolor='lightgray')
fig.update_layout(margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))


fig = px.histogram(
    df,
    x='Age',
    color='Frequently experiencing dizziness ',
    title='Proportion d\'individus souffrant fréquemment de vertiges selon l\'âge',
    labels={'Frequently experiencing dizziness ': 'Vertiges'}
)
fig.update_layout(barmode='group', xaxis_title='Age', yaxis_title='')
fig.update_yaxes(showgrid=True, gridcolor='lightgray')
fig.update_layout(margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))


df['inability_score'] = df[['Inability to use public transportation', 'Inability to descent and ascent a stair with 15 steps', 'Inability to clip own toenails']].sum(axis=1)
fig = px.bar(df, x='Age', y='inability_score', title='Inabilité de réaliser certaines tâches selon l\'âge')

fig.update_yaxes(showgrid=True, gridcolor='lightgray')
fig.update_layout(margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))


fig_bmi = px.histogram(df, x='BMI', title='Répartition de l\'IMC selon l\'âge')

# Supposons que df['BMI'] est votre DataFrame et que vous avez déjà filtré les valeurs manquantes
data_bmi = df['BMI'].dropna()

# Calcul de la densité du BMI
kde_bmi = gaussian_kde(data_bmi)
x_range_bmi = np.linspace(data_bmi.min(), data_bmi.max(), 500)  
density_bmi = kde_bmi(x_range_bmi) * len(data_bmi) * (data_bmi.max() - data_bmi.min()) / 500 

# Ajout de la courbe de densité au graphique fig_bmi
fig_bmi.add_trace(go.Scatter(x=x_range_bmi, y=density_bmi, name='Densité', line=dict(color='#FF5733', width=2), yaxis='y2'))

# Mise à jour des axes pour fig_bmi
fig_bmi.update_xaxes(title_text='', showgrid=False)
fig_bmi.update_yaxes(title_text='Nombre', side='left', showgrid=True, gridcolor='lightgray')

# Ajout d'un second axe Y pour la densité
fig_bmi.update_layout(
    yaxis2=dict(title='Densité', overlaying='y', side='right', showgrid=False),
    plot_bgcolor='white',
    bargap=0.2,
    title=dict(text="Répartition de l'IMC selon l'âge", x=0.5, font=dict(size=18)),
    legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01, bgcolor='rgba(255,255,255,0.9)')
)

# ------------------------------------------------------------------------ PARTIE CHUTES ------------------------------------------------------------------- # 

df['Use of a walking aid'] = df['Use of a walking aid'].map({1: 'Oui', 0: 'Non'})

falls_by_walking_aid = df.groupby('Use of a walking aid')['Number of falls in past year'].mean().reset_index()

fig_falls_by_walking_aid = px.bar(falls_by_walking_aid, 
                                  x='Use of a walking aid', 
                                  y='Number of falls in past year',
                                  title='Nombre moyen de chutes au cours de l\'année dernière en fonction de l\'utilisation d\'une aide à la marche',
                                  labels={'Use of a walking aid': 'Utilisation d\'une aide à la marche', 'Number of falls in past year': 'Nombre moyen de chutes'},
                                  color='Use of a walking aid',
                                  color_discrete_map={'Oui': '#FFD700', 'Non': '#800080'})  

fig_falls_by_walking_aid.update_layout(showlegend=False, margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))
fig_falls_by_walking_aid.update_yaxes(showgrid=True, gridcolor='lightgray')


falls_by_depression = df.groupby('Depression (GDS)')['Number of falls in past year'].mean().reset_index()

fig_falls_by_depression = px.bar(falls_by_depression, x='Depression (GDS)', y='Number of falls in past year',
                                  title='Nombre moyen de chutes au cours de l\'année dernière en fonction des niveaux de dépression',
                                  labels={'Depression (GDS)': 'Niveaux de dépression', 'Number of falls in past year': 'Nombre moyen de chutes'},
                                  color='Depression (GDS)',
                                  color_discrete_map={'low': 'green', 'medium': 'blue', 'high': 'red'}) 

fig_falls_by_depression.update_layout(showlegend=False, margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))
fig_falls_by_depression.update_traces(showlegend=False)
fig_falls_by_depression.update_yaxes(showgrid=True, gridcolor='lightgray')


falls_by_fear_of_falling = df.groupby('Fear of falling (FESi)')['Number of falls in past year'].mean().reset_index()

fig_falls_by_fear_of_falling = px.bar(falls_by_fear_of_falling, x='Fear of falling (FESi)', y='Number of falls in past year',
                                       title='Nombre moyen de chutes au cours de l\'année dernière selon la peur de tomber',
                                       labels={'Fear of falling (FESi)': 'Peur de tomber', 'Number of falls in past year': 'Nombre moyen de chutes'},
                                       color='Fear of falling (FESi)',
                                       color_discrete_sequence=px.colors.qualitative.Set1)

fig_falls_by_fear_of_falling.update_layout(showlegend=False, margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))
fig_falls_by_fear_of_falling.update_traces(showlegend=False)
fig_falls_by_fear_of_falling.update_yaxes(showgrid=True, gridcolor='lightgray')


bins = [65, 70, 75, 80, 85, 90, df['Age'].max() + 1] 
labels = ['65-69', '70-74', '75-79', '80-84', '85-89', '90+']
df['AgeGroup'] = pd.cut(df['Age'], bins=bins, labels=labels, right=False)

falls_by_age_group = df.groupby('AgeGroup')['Number of falls in past year'].mean().reset_index()

age_group_colors = {
    '65-69': '#FFD700',  
    '70-74': '#800080', 
    '75-79': '#FF69B4', 
    '80-84': '#FFA07A',
    '85-89': '#20B2AA',
    '90+': '#87CEFA'
}

fig_falls_by_age_group = px.bar(
    falls_by_age_group,
    x='AgeGroup',
    y='Number of falls in past year',
    title='Nombre moyen de chutes par groupe d\'âge',
    labels={'AgeGroup': 'Groupe d\'âge', 'Number of falls in past year': 'Nombre moyen de chutes'},
    color='AgeGroup',
    color_discrete_map=age_group_colors 
)

fig_falls_by_age_group.update_layout(showlegend=False, margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))
fig_falls_by_age_group.update_yaxes(showgrid=True, gridcolor='lightgray')

# -------------------------------------------------------------------------- PARTIE HTML ---------------------------------------------------------------- #
sidebar = html.Div(
    [
        html.H2('Dashboard de Qualité de Marche', style={'color': 'black'}),
        html.Hr(),
        dbc.Nav(
            [
                dbc.NavLink('Accueil', href='/', active='exact', className='nav-item-hover'),
                dbc.NavLink('Activité Physique et Mobilité', href='/Ac_Ph_M', active='exact', className='nav-item-hover'),
                dbc.NavLink('Démographie et Style de Vie', href='/D_S_V', active='exact', className='nav-item-hover'),
                dbc.NavLink('Santé et Bien-être', href='/S_B_E', active='exact', className='nav-item-hover'),
                dbc.NavLink('Chutes', href='/falls', active='exact', className='nav-item-hover'),
                dbc.NavLink('Graphique', href='/graph', active='exact', className='nav-item-hover'),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style={
        'position': 'fixed',
        'top': 0,
        'left': 0,
        'bottom': 0,
        'width': '16rem',
        'padding': '2rem 1rem',
        'background-color': '#f8f9fa',
    },
)

content = html.Div(id='page-content', style={'margin-left': '18rem', 'margin-right': '2rem', 'padding': '2rem 1rem'})

app.layout = html.Div([dcc.Location(id='url'), sidebar, content])

# Callback pour la navigation entre les pages
@app.callback(
    Output('page-content', 'children'),
    [Input('url', 'pathname')]
)

def render_page_content(pathname):

    if pathname == '/':
        return html.Div([
            html.H1('Accueil', style={'textAlign': 'center', 'color': '#39CCCC', 'marginBottom': '30px'}),
            html.P(
                "Bienvenue sur notre plateforme interactive dédiée à la visualisation de données de santé cruciales dans la prédiction du risque de chute chez les personnes âgées. Ce projet, conçu en collaboration avec le Centre Ingénierie et Santé, vise à exploiter des méthodes d'analyse descriptive avancées pour éclairer les facteurs influençant la qualité de marche et le bien-être des seniors. Grâce à l'utilisation de Dash pour Python, notre interface user-friendly vous permet d'explorer facilement des visualisations graphiques pertinentes, offrant des insights précieux sur des données essentielles à la compréhension et à la prévention des risques associés à l'âge. Découvrez avec nous comment la science des données peut améliorer la qualité de vie.",
                style={'color': '#39CCCC', 'fontSize': '20px', 'textAlign': 'justify', 'backgroundColor': '#001f3f'}
            ),
            dbc.Row(
                [dbc.Col(card, width=12) for card in cards], 
                className="mb-3 bg-dark text-light",
                style={'cursor': 'pointer'}
            )
        ])

    elif pathname == '/Ac_Ph_M':
        return html.Div([
            html.H1('Activité Physique et Mobilité', style={'textAlign': 'center', 'color': '#39CCCC', 'marginBottom': '30px'}),
            html.P(
                "Cette partie se concentre sur l'activité physique et la mobilité des patients de l\'étude.",
                style={'color': '#39CCCC', 'fontSize': '20px', 'textAlign': 'justify', 'backgroundColor': '#001f3f'}
            ),
            dbc.Row([
                dbc.Col([
                    html.Label("Sélectionnez le sexe :", style={'margin-bottom': '15px'}),
                    dbc.Checklist(
                        options=[
                            {"label": "Homme", "value": 1},
                            {"label": "Femme", "value": 0},
                        ],
                        value=[1, 0],  # Les deux options sont sélectionnées par défaut
                        id="gender-checkboxes-mobilite",
                        inline=True,  # Met les options côte à côte
                        switch=True,  # Utilise des "switches" au lieu de cases à cocher traditionnelles pour un style différent
                    ),
                ]),
                dbc.Col([
                    html.Label('Sélectionnez la tranche d\'âge :'),
                    html.Div(
                        dcc.RangeSlider(
                            id='age-range',
                            min=df['Age'].min(),
                            max=df['Age'].max(),
                            value=[df['Age'].min(), df['Age'].max()],
                            marks={str(age): str(age) for age in range(df['Age'].min(), df['Age'].max() + 1) if age % 5 == 0},
                            step=1,
                        ),
                        style={
                            'padding': '20px', 
                            'background-color': '#f8f9fa', 
                            'border-radius': '8px', 
                            'box-shadow': '0 2px 4px rgba(0,0,0,0.1)', 
                            'margin-top': '10px', 
                            'margin-bottom': '10px'
                        }
                    )
                ], width=6)
            ]),
            dbc.Row([
                dbc.Col(dcc.Graph(id='graph-locomotion-bouts', figure=fig_locomotion_bouts), width=6),
                dbc.Col(dcc.Graph(id='graph-posture-duration', figure=fig_posture_duration), width=6)
            ]),

            html.Div(style={'margin-top': '20px'}),

            dbc.Row([
                dbc.Col(dcc.Graph(id='graph-steps-vs-duration', figure=fig_steps_vs_duration), width=6),
                dbc.Col(dcc.Graph(id='graph-transfers', figure=fig_transfers), width=6) 
            ]),

            html.Div(style={'margin-top': '40px'}),

            dbc.Row([
                dcc.Dropdown(
                    id='x-axis-dropdown',
                    options=dropdown_options_mobility,
                    value=dropdown_options_mobility[0],  
                    className='form-control',
                    style={'padding': '10px'}
                ),
                dcc.Dropdown(
                    id='y-axis-dropdown',
                    options=dropdown_options_mobility,
                    value=dropdown_options_mobility[1], 
                    className='form-control',
                    style={'padding': '10px'}
                )
            ]),

            dbc.Row([
                dcc.Graph(id='dynamic-graph')
            ])
        ])


    elif pathname == '/D_S_V':
        return html.Div([
            html.H1('Démographie et Style de Vie', style={'textAlign': 'center', 'color': '#39CCCC', 'marginBottom': '30px'}),
            html.P(
                "Cette partie se focalise sur la démographie et le style de vies des personnes du jeu de données.",
                style={'color': '#39CCCC', 'fontSize': '20px', 'textAlign': 'justify', 'backgroundColor': '#001f3f'}
            ),
            dbc.Row([
                dbc.Col([
                    html.Label("Sélectionnez le sexe :", style={'margin-bottom': '15px'}),
                    dbc.Checklist(
                        options=[
                            {"label": "Homme", "value": 1},
                            {"label": "Femme", "value": 0},
                        ],
                        value=[1, 0],  # Les deux options sont sélectionnées par défaut
                        id="gender-checkboxes-demographie",
                        inline=True,  # Met les options côte à côte
                        switch=True,  # Utilise des "switches" au lieu de cases à cocher traditionnelles pour un style différent
                    ),
                ]),

                dbc.Col([
                    html.Label('Sélectionnez la tranche d\'âge :'),
                    html.Div(
                        dcc.RangeSlider(
                            id='age-range-slider',
                            min=df['Age'].min(),
                            max=df['Age'].max(),
                            value=[df['Age'].min(), df['Age'].max()],
                            marks={str(age): str(age) for age in range(df['Age'].min(), df['Age'].max() + 1) if age % 5 == 0},
                            step=1,
                        ),
                        style={
                            'padding': '20px', 
                            'background-color': '#f8f9fa', 
                            'border-radius': '8px', 
                            'box-shadow': '0 2px 4px rgba(0,0,0,0.1)', 
                            'margin-top': '10px', 
                            'margin-bottom': '10px'
                        }
                    )
                ], width=6)
            ]),
    
            dbc.Row([
                dbc.Col(dcc.Graph(id='graph-age-distribution'), width=6),
                dbc.Col(dcc.Graph(id='graph-living-situation-pie'), width=6),
            ]),

            html.Div(style={'margin-top': '20px'}),

            dbc.Row([
                dbc.Col(dcc.Graph(id='graph-education-level-pie'), width=6),
                dbc.Col(dcc.Graph(id='graph-living-with-partner-pie'), width=6)
            ]),

            html.Div(style={'margin-top': '40px'}),
            
            dbc.Row([
            dcc.Dropdown(
                id='x-axis-dropdown',
                options=dropdown_options_Demographie,
                value=dropdown_options_Demographie[0],  
                className='form-control',
                style={'padding': '10px'}
            ),

            dcc.Dropdown(
                id='y-axis-dropdown',
                options=dropdown_options_Demographie,
                value=dropdown_options_Demographie[1], 
                className='form-control',
                style={'padding': '10px'}
            ),]),

            dbc.Row([
            dcc.Graph(id='dynamic-graph-demo')
                ])
        ])

    elif pathname == '/S_B_E':
        return html.Div([
            html.H1('Santé et Bien-être', style={'textAlign': 'center', 'color': '#39CCCC', 'marginBottom': '30px'}),
            html.P(
                "Cette partie est basée sur la santé et le bien-être.",
                style={'color': '#39CCCC', 'fontSize': '20px', 'textAlign': 'justify', 'backgroundColor': '#001f3f'}
            ),
            dbc.Row([
                dbc.Col([
                    html.Label("Sélectionnez le sexe :", style={'margin-bottom': '15px'}),
                    dbc.Checklist(
                        options=[
                            {"label": "Homme", "value": 1},
                            {"label": "Femme", "value": 0},
                        ],
                        value=[1, 0],  # Les deux options sont sélectionnées par défaut
                        id="gender-checkboxes-sante",
                        inline=True,  # Met les options côte à côte
                        switch=True,  # Utilise des "switches" au lieu de cases à cocher traditionnelles pour un style différent
                    ),
                ]),
                dbc.Col([
                    html.Label('Sélectionnez la tranche d\'âge :'),
                    html.Div(
                        dcc.RangeSlider(
                            id='age-dropdown-health',
                            min=df['Age'].min(),
                            max=df['Age'].max(),
                            value=[df['Age'].min(), df['Age'].max()],
                            marks={str(age): str(age) for age in range(df['Age'].min(), df['Age'].max() + 1) if age % 5 == 0},
                            step=1,
                        ),
                        style={
                            'padding': '20px', 
                            'background-color': '#f8f9fa', 
                            'border-radius': '8px', 
                            'box-shadow': '0 2px 4px rgba(0,0,0,0.1)', 
                            'margin-top': '10px', 
                            'margin-bottom': '10px'
                        }
                    )
                ], width=6)
            ]),
            
            dbc.Row([
                dbc.Col(dcc.Graph(id='graph-cognitive-function-by-age'), width=6),
                dbc.Col(dcc.Graph(id='graph-depression-levels'), width=6),
            ]),

            html.Div(style={'margin-top': '20px'}),

            dbc.Row([
                dbc.Col(dcc.Graph(id='graph-health-condition-prevalence'), width=6),
                dbc.Col(dcc.Graph(id='graph-physical-activity-health-outcomes'), width=6)
            ]),
        ])
    
    elif pathname == '/falls':
        return html.Div([
            html.H1('Chutes', style={'textAlign': 'center', 'color': '#39CCCC', 'marginBottom': '30px'}),
            html.P(
                "Cette partie se consacre à l'analyse des chutes selon différentes variables.",
                style={'color': '#39CCCC', 'fontSize': '20px', 'textAlign': 'justify', 'backgroundColor': '#001f3f'}
            ),
        
            dbc.Row([
                    dbc.Col(dcc.Graph(figure=fig_falls_by_walking_aid), width=6),
                    dbc.Col(dcc.Graph(figure=fig_falls_by_depression), width=6)
            ]),

            html.Div(style={'margin-top': '20px'}),

            dbc.Row([
                    dbc.Col(dcc.Graph(figure=fig_falls_by_fear_of_falling), width=6),
                    dbc.Col(dcc.Graph(figure=fig_falls_by_age_group), width=6)
                ])
        ])

    elif pathname == '/graph':
        return html.Div([
            html.H1('Création de graphique', style={'textAlign': 'center', 'color': '#39CCCC', 'marginBottom': '30px'}),
            html.P(
                "Cette partie est réservée pour vous : sentez vous libre de créer le graphique de votre choix !",
                style={'color': '#39CCCC', 'fontSize': '20px', 'textAlign': 'justify', 'backgroundColor': '#001f3f'}
            ),
            html.Label("Selectionnez le type de graphique : "),
            dcc.Dropdown(
                id='graph-type-dropdown',
                options=[
                    {'label': 'Bar Chart', 'value': 'bar'},
                    {'label': 'Line Chart', 'value': 'line'},
                    {'label': 'Pie Chart', 'value': 'pie'},
                    {'label': 'Scatter Plot', 'value': 'scatter'},
                    {'label': 'Histogram', 'value': 'histogram'},
                    {'label': 'Box Plot', 'value': 'box'},
                    {'label': 'Violin Plot', 'value': 'violin'},
                    {'label': 'Heatmap', 'value': 'heatmap'},
                    {'label': 'Area Chart', 'value': 'area'}
                ],
                value='bar',
                className='form-control',
                style={'padding': '10px'}
            ),
            dbc.Row([
                dbc.Col([
                    html.Label('Selectionner la variable X :'),
                    dcc.Dropdown(
                        id='y-var-dropdown',
                        options=[{'label': i, 'value': i} for i in df.columns],
                        value=df.columns[0],
                        placeholder="Sélectionnez la variable X",
                        className='form-control',
                        style={'padding': '10px'}
                        )
                    ], width=6),
                dbc.Col([
                    html.Label('Sélectionnez la variable Y :'),
                    dcc.Dropdown(
                        id='x-var-dropdown',
                        options=[{'label': i, 'value': i} for i in df.columns],
                        value=df.columns[1],
                        placeholder="Sélectionnez la variable Y",
                        className='form-control',
                        style={'padding': '10px'}
                        )
                    ], width=6),

            html.Div(style={'margin-top': '20px'}),
                        
            html.Button('Générer le graphique', id='generate-graph-button', style={'backgroundColor': '#007bff', 'color': 'white', 'fontSize': '14px', 'padding': '5px 10px', 'border': 'none', 'borderRadius': '5px'}),


            html.Div(style={'margin-top': '20px'}),
            
            dcc.Graph(id='custom-graph')
        ])
    ])
    # Si l'utilisateur tente d'atteindre une URL différente, retourner un message d'erreur
    return dbc.Jumbotron(
        [
            html.H1('404: Not found', className='text-danger'),
            html.Hr(),
            html.P(f'The pathname {pathname} was not recognised...'),
        ]
    )

# -------------------------------------------------Partie Activité physique et Mobilité -------------------------------------------------------- # 
@app.callback(
    Output('dynamic-graph', 'figure'),
    [Input('x-axis-dropdown', 'value'),
     Input('y-axis-dropdown', 'value')]
)

def update_graph(selected_x, selected_y):
    # Créer un graphique basé sur les sélections
    fig = px.scatter(df, x=selected_x, y=selected_y)
        
    return fig

# Callback pour mettre à jour le graphique des transferts
@app.callback(
    Output('graph-transfers', 'figure'),  # Cible le graphique spécifique par son ID
    [Input('gender-checkboxes-mobilite', 'value'),  # Écoute les sélections de la liste déroulante du sexe
     Input('age-range', 'value')]  # Écoute les sélections de la liste déroulante des tranches d'âge
)

def update_transfers_graph(selected_gender, age_range):
    
    if selected_gender:  # Vérifie si la liste n'est pas vide
        filtered_df = df[df['Male gender'].isin(selected_gender)]
    else:
        filtered_df = df  # Si aucune sélection, utiliser toutes les données

    # Filtrer les données en fonction de l'âge
    filtered_df = filtered_df[(filtered_df['Age'] >= age_range[0]) & (filtered_df['Age'] <= age_range[1])]

    # Réappliquer la logique pour binning basé sur 'Number of transfers'
    # Assurez-vous que cette logique correspond à la manière dont vous avez initialisé 'Transfers_Category' initialement
    labels = ['Faible', 'Moyen', 'Elevé']  # Example labels for three categories

    # Ensuring 'bins' creates three intervals, which matches the number of 'labels'
    bins = [filtered_df['Number of transfers'].min(), low_threshold, high_threshold, filtered_df['Number of transfers'].max()]
    filtered_df['Transfers_Category'] = pd.cut(filtered_df['Number of transfers'], bins=bins, labels=labels, include_lowest=True)
    transfers_binned = filtered_df['Transfers_Category'].value_counts().sort_index()

    # Recréez le graphique à barres avec les données filtrées
    fig = px.bar(
        x=transfers_binned.index, 
        y=transfers_binned.values,
        title='Nombre de transferts par catégorie',
        color_discrete_sequence=['#EF553B']
    )
    fig.update_layout(
        xaxis_title="",
        yaxis_title="",
        xaxis={'showticklabels': True},
        yaxis={'showticklabels': True}
    )

    fig.update_yaxes(showgrid=True, gridcolor='lightgray')
    fig.update_layout(margin=dict(t=60), plot_bgcolor='white', title_x=0.5, title_font=dict(size=18))
    fig.update_layout(legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01, bgcolor='rgba(255,255,255,0.9)'))

    return fig


@app.callback(
    Output('graph-locomotion-bouts', 'figure'),
    [Input('gender-checkboxes-mobilite', 'value'),
     Input('age-range', 'value')]
)

def update_locomotion_bouts(selected_gender, age_range):

    if selected_gender:  # Vérifie si la liste n'est pas vide
        filtered_df = df[df['Male gender'].isin(selected_gender)]
    else:
        filtered_df = df  # Si aucune sélection, utiliser toutes les données

    # Filtrer les données en fonction de l'âge
    filtered_df = filtered_df[(filtered_df['Age'] >= age_range[0]) & (filtered_df['Age'] <= age_range[1])]

    data = filtered_df['Number of locomotion bouts'].dropna() 

    kde = gaussian_kde(data)
    x_range = np.linspace(data.min(), data.max(), 500)
    density = kde(x_range) * len(data) * (data.max() - data.min()) / 500 


    fig_locomotion_bouts = go.Figure()
    fig_locomotion_bouts.add_trace(go.Histogram(x=data, name='Nombre d\'épisodes', marker_color='#007BFF', opacity=0.75))
    fig_locomotion_bouts.add_trace(go.Scatter(x=x_range, y=density, name='Densité', line=dict(color='#FF5733', width=2), yaxis='y2'))

    fig_locomotion_bouts.update_xaxes(title_text='', showgrid=False)
    fig_locomotion_bouts.update_yaxes(title_text='Nombre', side='left', showgrid=True, gridcolor='lightgray')

    fig_locomotion_bouts.update_layout(
        yaxis2=dict(title='Densité', overlaying='y', side='right', showgrid=False),
        title=dict(text='Nombre d\'épisodes de locomotion', x=0.5, font=dict(size=18)),
        plot_bgcolor='white',
        bargap=0.2
    )

    fig_locomotion_bouts.update_layout(legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01, bgcolor='rgba(255,255,255,0.9)'))

    return fig_locomotion_bouts

@app.callback(
    Output('graph-posture-duration', 'figure'),
    [Input('gender-checkboxes-mobilite', 'value'),
     Input('age-range', 'value')]
)

def update_posture_duration(selected_gender, age_range):
    
    if selected_gender:  # Vérifie si la liste n'est pas vide
        filtered_df = df[df['Male gender'].isin(selected_gender)]
    else:
        filtered_df = df  # Si aucune sélection, utiliser toutes les données

    # Filtrer les données en fonction de l'âge
    filtered_df = filtered_df[(filtered_df['Age'] >= age_range[0]) & (filtered_df['Age'] <= age_range[1])]
        
    fig = go.Figure()
    fig.add_trace(go.Pie(labels=['Debout', 'Assis', 'Couché'],
                                      values=[filtered_df['Duration of standing'].sum(),
                                              filtered_df['Duration of sitting'].sum(),
                                              filtered_df['Duration of lying'].sum()],
                                      hole=.3))

    fig.update_layout(title_text='Répartition des durées des postures')
    fig.update_layout(margin=dict(t=60), plot_bgcolor='white', title_x=0.5, title_font=dict(size=18))

    fig.update_layout(legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01, bgcolor='rgba(255,255,255,0.9)'))
    
    return fig

@app.callback(
    Output('graph-steps-vs-duration', 'figure'),
    [Input('gender-checkboxes-mobilite', 'value'),
     Input('age-range', 'value')]
)

def update_steps_vs_duration(selected_gender, age_range):
    
    if selected_gender:  # Vérifie si la liste n'est pas vide
        filtered_df = df[df['Male gender'].isin(selected_gender)]
    else:
        filtered_df = df  # Si aucune sélection, utiliser toutes les données

    # Filtrer les données en fonction de l'âge
    filtered_df = filtered_df[(filtered_df['Age'] >= age_range[0]) & (filtered_df['Age'] <= age_range[1])]
        
    fig = px.scatter(filtered_df, x='Number of strides', y='Duration of locomotion',
                                   title='Nombre de pas en fonction de la durée de locomotion',
                                   color_discrete_sequence=['#00CC96'])
    
    fig.update_xaxes(title_text='Nombre de pas', showgrid=False)
    fig.update_yaxes(title_text='Durée de locomotion', showgrid=True, gridcolor='lightgray')
    
    fig.update_layout(margin=dict(t=60), plot_bgcolor='white', title_x=0.5, title_font=dict(size=18))
    fig.update_layout(legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01, bgcolor='rgba(255,255,255,0.9)'))

    return fig


# ------------------------------------------------- Partie Démographie et Style de vie -------------------------------------------------------- # 
@app.callback(
    Output('graph-age-distribution', 'figure'),
    [Input('gender-checkboxes-demographie', 'value'),
     Input('age-range-slider', 'value')]
)

def update_age_distribution(selected_gender, age_range):
    
    if selected_gender:  # Vérifie si la liste n'est pas vide
        filtered_df = df[df['Male gender'].isin(selected_gender)]
    else:
        filtered_df = df  # Si aucune sélection, utiliser toutes les données

    # Filtrer les données en fonction de l'âge
    filtered_df = filtered_df[(filtered_df['Age'] >= age_range[0]) & (filtered_df['Age'] <= age_range[1])]

    # Préparation des données pour l'histogramme de l'âge
    data_age = filtered_df['Age'].dropna() 

    # Calcul de la densité pour l'âge
    kde_age = gaussian_kde(data_age)
    x_range_age = np.linspace(data_age.min(), data_age.max(), 500)
    density_age = kde_age(x_range_age) * len(data_age) * (data_age.max() - data_age.min()) / 500 

    # Création de la figure pour l'âge avec l'histogramme et la courbe de densité
    fig_age_distribution = go.Figure()

    # Ajout de l'histogramme pour l'âge
    fig_age_distribution.add_trace(go.Histogram(x=data_age, name='Distribution de l\'âge', marker_color='#007BFF', opacity=0.75))

    # Ajout de la courbe de densité pour l'âge sur un axe des ordonnées secondaire
    fig_age_distribution.add_trace(go.Scatter(x=x_range_age, y=density_age, name='Densité', line=dict(color='#FF5733', width=2), yaxis='y2'))

    # Mise à jour des paramètres d'axe et de style pour la distribution de l'âge
    fig_age_distribution.update_xaxes(title_text='Âge', showgrid=False)
    fig_age_distribution.update_yaxes(title_text='Nombre', side='left', showgrid=True, gridcolor='lightgray')

    fig_age_distribution.update_layout(
        yaxis2=dict(title='Densité', overlaying='y', side='right', showgrid=False),
        title=dict(text='Distribution de l\'âge', x=0.5, font=dict(size=18)),
        plot_bgcolor='white',
        bargap=0.2
    )
    fig_age_distribution.update_layout(legend=dict(yanchor="top", y=0.99, xanchor="right", x=0.01, bgcolor='rgba(255,255,255,0.9)'))

    return fig_age_distribution

@app.callback(
    Output('graph-living-situation-pie', 'figure'),
    [Input('gender-checkboxes-demographie', 'value'),
     Input('age-range-slider', 'value')]
)

def update_living_situation(selected_gender, age_range):
    
    if selected_gender:  # Vérifie si la liste n'est pas vide
        filtered_df = df[df['Male gender'].isin(selected_gender)]
    else:
        filtered_df = df  # Si aucune sélection, utiliser toutes les données

    # Filtrer les données en fonction de l'âge
    filtered_df = filtered_df[(filtered_df['Age'] >= age_range[0]) & (filtered_df['Age'] <= age_range[1])]
    
    living_situation_counts = filtered_df["Living independently (0=yes, 1=assited living, 2=residential care)"].value_counts()
    labels = ['Indépendant', 'Vie Assistée', 'Soins Résidentiels']
    values = living_situation_counts.values

    fig_living_situation = go.Figure(data=[go.Pie(labels=labels, values=values)])
    fig_living_situation.update_layout(title_text='Situation de Vie')

    fig_living_situation.update_layout(margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))


    return fig_living_situation

@app.callback(
    Output('graph-education-level-pie', 'figure'),
    [Input('gender-checkboxes-demographie', 'value'),
     Input('age-range-slider', 'value')]
)

def update_education_level(selected_gender, age_range):
    
    if selected_gender:  # Vérifie si la liste n'est pas vide
        filtered_df = df[df['Male gender'].isin(selected_gender)]
    else:
        filtered_df = df  # Si aucune sélection, utiliser toutes les données

    # Filtrer les données en fonction de l'âge
    filtered_df = filtered_df[(filtered_df['Age'] >= age_range[0]) & (filtered_df['Age'] <= age_range[1])]
    
    education_counts = filtered_df["Followed higher education (more than 11 years)"].value_counts().rename({0: 'Pas d\'éducation supérieure', 1: 'Éducation supérieure'})
    labels = education_counts.index 
    values = education_counts.values

    fig_education_level = px.pie(names=labels, values=values, title="Éducation")
    fig_education_level.update_traces(textinfo='percent', textfont_size=10)
    fig_education_level.update_layout(showlegend=True, plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))
    

    return fig_education_level

@app.callback(
    Output('graph-living-with-partner-pie', 'figure'),
    [Input('gender-checkboxes-demographie', 'value'),
     Input('age-range-slider', 'value')]
)

def update_living_with_partner(selected_gender, age_range):
    
    if selected_gender:  # Vérifie si la liste n'est pas vide
        filtered_df = df[df['Male gender'].isin(selected_gender)]
    else:
        filtered_df = df  # Si aucune sélection, utiliser toutes les données

    # Filtrer les données en fonction de l'âge
    filtered_df = filtered_df[(filtered_df['Age'] >= age_range[0]) & (filtered_df['Age'] <= age_range[1])]
    
    partner_counts = filtered_df["Living with partner"].value_counts(normalize=True)
    
    fig_living_with_partner = px.pie(
        values=partner_counts.values, 
        names=partner_counts.index.map({0: 'Sans Partenaire', 1: 'Avec Partenaire'}), 
        title="Situation conjugale"
    )

    fig_living_with_partner.update_layout(margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))


    return fig_living_with_partner

@app.callback(
    Output('dynamic-graph-demo', 'figure'),
    [Input('x-axis-dropdown', 'value'),
     Input('y-axis-dropdown', 'value')]
)

def update_graph(selected_x, selected_y):
    if not isinstance(selected_y, list):
        selected_y = [selected_y]

    fig = px.bar(df, x=selected_x, y=selected_y, barmode='group')
    return fig


# ------------------------------------------------- Partie Santé et Bien être -------------------------------------------------------- # 
@app.callback(
    Output('graph-cognitive-function-by-age', 'figure'),
    [Input('gender-checkboxes-sante', 'value'), 
     Input('age-dropdown-health', 'value')] 
)
def update_cognitive_function_scatter(selected_gender, age_range):
    # Vérifie si la liste n'est pas vide
    if selected_gender:
        filtered_df = df[df['Male gender'].isin(selected_gender)]
    else:
        filtered_df = df  # Si aucune sélection, utiliser toutes les données

    # Filtrer les données en fonction de l'âge
    filtered_df = filtered_df[(filtered_df['Age'] >= age_range[0]) & (filtered_df['Age'] <= age_range[1])]

    fig = px.scatter(
        filtered_df,
        x='Age',
        y='Cognitive function (MMSE score)',
        title='Fonction cognitive (score MMSE) selon l\'âge',
        color='Age',
        labels={'Age': 'Age', 'Cognitive function (MMSE score)': 'MMSE Score'}
    )

    fig.update_yaxes(showgrid=True, gridcolor='lightgray')
    fig.update_layout(margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=20))

    return fig


@app.callback(
    Output('graph-depression-levels', 'figure'),
    [Input('gender-checkboxes-sante', 'value'), 
     Input('age-dropdown-health', 'value')] 
)
def update_dizziness(selected_gender, age_range):

    if selected_gender:  # Vérifie si la liste n'est pas vide
        filtered_df = df[df['Male gender'].isin(selected_gender)]
    else:
        filtered_df = df  # Si aucune sélection, utiliser toutes les données

    # Filtrer les données en fonction de l'âge
    filtered_df = filtered_df[(filtered_df['Age'] >= age_range[0]) & (filtered_df['Age'] <= age_range[1])]

    fig = px.histogram(
        filtered_df,
        x='Age',
        color='Frequently experiencing dizziness ',
        title='Proportion d\'individus souffrant fréquemment de vertiges selon l\'âge',
        labels={'Frequently experiencing dizziness ': 'Vertige'}
    )
    fig.update_layout(barmode='group', xaxis_title='Age', yaxis_title='')

    fig.update_yaxes(showgrid=True, gridcolor='lightgray')
    fig.update_layout(margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))

    return fig

@app.callback(
    Output('graph-health-condition-prevalence', 'figure'),
    [Input('gender-checkboxes-sante', 'value'), 
     Input('age-dropdown-health', 'value')] 
)
def update_inability(selected_gender, age_range):

    if selected_gender:  # Vérifie si la liste n'est pas vide
        filtered_df = df[df['Male gender'].isin(selected_gender)]
    else:
        filtered_df = df  # Si aucune sélection, utiliser toutes les données

    # Filtrer les données en fonction de l'âge
    filtered_df = filtered_df[(filtered_df['Age'] >= age_range[0]) & (filtered_df['Age'] <= age_range[1])]

    filtered_df['inability_score'] = df[['Inability to use public transportation', 'Inability to descent and ascent a stair with 15 steps', 'Inability to clip own toenails']].sum(axis=1)
    fig= px.bar(filtered_df, x='Age', y='inability_score', title='Inabilité de réaliser certaines tâches selon l\'âge')

    fig.update_yaxes(showgrid=True, gridcolor='lightgray')
    fig.update_layout(margin=dict(t=60), plot_bgcolor='rgba(0,0,0,0)', title_x=0.5, title_font=dict(size=18))

    return fig

@app.callback(
    Output('graph-physical-activity-health-outcomes', 'figure'),
    [Input('gender-checkboxes-sante', 'value'), 
     Input('age-dropdown-health', 'value')] 
)
def update_bmi_plot(selected_gender, age_range):

    if selected_gender:  # Vérifie si la liste n'est pas vide
        filtered_df = df[df['Male gender'].isin(selected_gender)]
    else:
        filtered_df = df  # Si aucune sélection, utiliser toutes les données

    # Filtrer les données en fonction de l'âge
    filtered_df = filtered_df[(filtered_df['Age'] >= age_range[0]) & (filtered_df['Age'] <= age_range[1])]

    fig_bmi = px.histogram(filtered_df, x='BMI', title='Répartition de l\'IMC selon l\'âge')

    # Supposons que df['BMI'] est votre DataFrame et que vous avez déjà filtré les valeurs manquantes
    data_bmi = filtered_df['BMI'].dropna()

    # Calcul de la densité du BMI
    kde_bmi = gaussian_kde(data_bmi)
    x_range_bmi = np.linspace(data_bmi.min(), data_bmi.max(), 500)  
    density_bmi = kde_bmi(x_range_bmi) * len(data_bmi) * (data_bmi.max() - data_bmi.min()) / 500 

    # Ajout de la courbe de densité au graphique fig_bmi
    fig_bmi.add_trace(go.Scatter(x=x_range_bmi, y=density_bmi, name='Densité', line=dict(color='#FF5733', width=2), yaxis='y2'))

    # Mise à jour des axes pour fig_bmi
    fig_bmi.update_xaxes(title_text='', showgrid=False)
    fig_bmi.update_yaxes(title_text='Nombre', side='left', showgrid=True, gridcolor='lightgray')

    # Ajout d'un second axe Y pour la densité
    fig_bmi.update_layout(
        yaxis2=dict(title='Densité', overlaying='y', side='right', showgrid=False),
        plot_bgcolor='white',
        bargap=0.2,
        title=dict(text="Répartition de l'IMC selon l'âge", x=0.5, font=dict(size=18)),
        legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01, bgcolor='rgba(255,255,255,0.9)')
    )

    return fig_bmi


# ------------------------------------------------- Partie Création de graphique -------------------------------------------------------- # 
@app.callback(
    Output('custom-graph', 'figure'),
    [Input('generate-graph-button', 'n_clicks')],
    [State('graph-type-dropdown', 'value'),
     State('x-var-dropdown', 'value'),
     State('y-var-dropdown', 'value')]
)
def update_graph(n_clicks, graph_type, x_var, y_var):
    if n_clicks is None:
        raise dash.exceptions.PreventUpdate
    
    if graph_type == 'bar':
        fig = px.bar(df, x=x_var, y=y_var)
    elif graph_type == 'line':
        fig = px.line(df, x=x_var, y=y_var)
    elif graph_type == 'pie':
        fig = px.pie(df, names=x_var, values=y_var)
    elif graph_type == 'scatter':
        fig = px.scatter(df, x=x_var, y=y_var)
    elif graph_type == 'histogram':
        fig = px.histogram(df, x=x_var)
    elif graph_type == 'box':
        fig = px.box(df, x=x_var, y=y_var)
    elif graph_type == 'violin':
        fig = px.violin(df, x=x_var, y=y_var)
    elif graph_type == 'heatmap':
        fig = px.imshow(df[[x_var, y_var]].corr())
    elif graph_type == 'area':
        fig = px.area(df, x=x_var, y=y_var)

    return fig

if __name__ == '__main__':
    app.run_server(debug=True, port=8053)