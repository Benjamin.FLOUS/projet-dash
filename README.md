# Projet 

## Conception et développement d’une plateforme de visualisation des données avec Dash

### Auteurs

* **François BARBIER** 
* **Benjamin FLOUS**


### Pour lancer la plateforme, lancer la commande suivante dans un terminal : python3 main.py puis effectuer CTRL + click sur l'url qui apparait dans votre terminal.